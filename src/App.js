import React from 'react';
import './App.css';
import Form from './components/Form'
import Main from './components/Main'
import NavBar from './components/NavBar'


class App extends React.Component {

  state = {
    cities: [],
    error: false,
    loaded: false,
    timedOut: false,
  }

  toCountryCode(country) {
    if (country === "Poland")
      return "PL"

    if (country === "Germany")
      return "DE"

    if (country === "France")
      return "FR"

    if (country === "Spain")
      return "ES"

    return "Wrong country! "
  }

  startFetch() {
    this.setState({
      loaded: true,
      error: false,
      timedOut: false,
    })
  }

  endFetch() {
    this.setState({
      loaded: false,
      error: false,
    })
  }


  timeOut() {
    alert("request timed out");
    this.setState({
      loaded: false,
      error: false,
      timedOut: true,
    })
  }

  getNewData = async (e) => {
    e.preventDefault()

    this.startFetch();

    const country = this.toCountryCode(e.target.elements.country.value)
    const parameter = e.target.elements.parameter.value ? e.target.elements.parameter.value : "pm25"
    const accuracy = e.target.elements.accuracy.value <= 1000 ? e.target.elements.accuracy.value >= 10 ? e.target.elements.accuracy.value : 100 : 100


    localStorage.setItem('country', e.target.elements.country.value)
    localStorage.setItem('parameter', parameter)
    localStorage.setItem('accuracy', accuracy)

    const date = new Date();
    const day = date.getUTCDate() ? date.getUTCDate() : date.getUTCDate();
    const month = date.getUTCMonth() + 1;
    const year = date.getFullYear();


    let url = "https://api.openaq.org/v1/measurements?limit=" + accuracy + "&date_from=" + year + "-" + month + "-" + day + "&country=" + country + "&order_by=value&sort=desc&parameter=" + parameter

    let requestTime = setTimeout(() => this.timeOut(), 20000);

    let api_call;
    try {
      api_call = await fetch(url);
    }
    catch (ex) {
      this.setState({
        error: ex,
        loaded: false,
      })
    }

    if (country !== "Wrong country! ") {
      clearTimeout(requestTime);
      if (!this.state.error && !this.state.timedOut) {
        let data = await api_call.json();

        const result = [];

        for (const item of data.results) {
          result.push({
            location: item.location,
            city: item.city,
            result: item.value,
            parameter: item.parameter,
            date: item.date.local,
            coordinates: item.coordinates,
            country: item.country,
            unit: item.unit,
          });
        }

        const filter = result.reduce((acc, d) => {
          const found = acc.find(a => a.city === d.city);
          if (!found) {
            acc.push({
              location: d.location,
              city: d.city,
              parameter: d.parameter,
              date: d.date,
              coordinates: d.coordinates,
              country: d.country,
              unit: d.unit,
              result: d.result,
              count: 1
            })
          }
          else {
            found.result += d.result
            found.count += 1
          }
          return acc;
        }, []).map(d => {
          d.result /= d.count
          return ({
            city: d.city,
            result: d.result,
            parameter: d.parameter,
            date: d.date,
            coordinates: d.coordinates,
            country: d.country,
            unit: d.unit,
            count: d.count
          })
        }).sort((a, b) => (a.result < b.result) ? 1 : -1);

        this.setState({
          cities: filter.slice(0, 10),
          loaded: false,
        })
      }

    } else {
      clearTimeout(requestTime);
      alert("Wrong Country!");
    }

    this.endFetch();
  }

  getArchiveData = async (e) => {
    e.preventDefault()
    this.startFetch();

    const country = this.toCountryCode(e.target.elements.country.value)
    const parameter = e.target.elements.parameter.value ? e.target.elements.parameter.value : "pm25"
    const accuracy = e.target.elements.accuracy.value <= 1000 ? e.target.elements.accuracy.value >= 10 ? e.target.elements.accuracy.value : 100 : 100

    localStorage.setItem('country', e.target.elements.country.value)
    localStorage.setItem('parameter', parameter)
    localStorage.setItem('accuracy', accuracy)

    let url = "https://api.openaq.org/v1/latest?&limit=" + accuracy + "&country=" + country + "&order_by=parameterments[0].value&sort=desc&parameter=" + parameter

    let requestTime = setTimeout(() => this.timeOut(), 20000);
    let api_call;
    try {
      api_call = await fetch(url);
    }
    catch (ex) {
      this.setState({
        error: ex,
        loaded: false,
      })
    }

    if (country !== "Wrong country! ") {
      clearTimeout(requestTime);
      if (!this.state.error && !this.state.timedOut) {

        let data = await api_call.json()

        const result = [];

        const map = new Map();
        for (const item of data.results) {
          if (!map.has(item.location)) {
            map.set(item.location, true);
            result.push({
              city: item.city,
              result: item.measurements[0].value,
              parameter: item.measurements[0].parameter,
              date: item.measurements[0].lastUpdated,
              coordinates: item.coordinates,
              country: item.country,
              unit: item.measurements[0].unit,
            });
          }
        }

        const filtered = result.reduce((acc, d) => {
          const found = acc.find(a => a.city === d.city);
          if (!found) {
            acc.push({
              city: d.city,
              parameter: d.parameter,
              date: d.date,
              coordinates: d.coordinates,
              country: d.country,
              unit: d.unit,
              result: d.result,
              count: 1
            })
          }
          else {
            found.result += d.result
            found.count += 1
          }
          return acc;
        }, []).map(d => {
          d.result /= d.count
          return ({
            city: d.city,
            result: d.result,
            parameter: d.parameter,
            date: d.date,
            coordinates: d.coordinates,
            country: d.country,
            unit: d.unit,
            count: d.count
          })
        }).sort((a, b) => (a.result < b.result) ? 1 : -1);

        this.setState({
          cities: filtered.slice(0, 10),
          loaded: false,
        })
      }
    }
    else {
      clearTimeout(requestTime);
      alert("Wrong Country!");
    }
    this.endFetch();
  }

  render() {

    const results = this.state.cities.map(item => (
      <Main key={item.city} item={item} />
    ))
    return (
      <div style={{ position: "relative" }}>
        <div>
          <NavBar />
        </div>
        <div className="center form">
          <Form getArchiveData={this.getArchiveData} getNewData={this.getNewData} loaded={this.state.loaded} />
        </div>
        <div className="mainapp">
          {results}
        </div>
      </div>
    );
  }
}
export default App;
