import React from 'react'
import Accordion from './Accordion'
class Main extends React.Component {

    state = {
        wikidata: null,
        city: null,
        country: null,
        longitude: null,
        latitude: null,
        date: null,
        count: null,
    }
    getWiki = async (_city) => {
        const city = _city
        const url = `https://en.wikipedia.org/w/api.php?action=query&titles=${city}&prop=extracts&explaintext=true&exsentences=1&indexpageids=&format=json&origin=*`
        const api_call = await fetch(url);
        const data = await api_call.json()
        const pageID = data.query.pageids
        this.setState({
            wikidata: data.query.pages[pageID].extract,
        })
    }

    componentWillMount() {
        this._isMounted = true;
        this.getWiki(this.props.item.city)
        this.setState({
            city: this.props.item.city,
            country: this.props.item.country,
            longitude: this.props.item.coordinates ? this.props.item.coordinates.longitude : null,
            latitude: this.props.item.coordinates ? this.props.item.coordinates.latitude : null,
            date: this.props.item.date,
            unit: this.props.item.unit,
            count: this.props.item.count,
        })
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    valueTextColor(_val, _parameter) {

        if (_parameter === "pm25") {
            if (_val > 50)
                return "danger-level limit"
            else if (_val > 37.5)
                return "warning-level limit"
            else return "good-level limit"
        }
        if (_parameter === "pm10") {
            if (_val > 100)
                return "danger-level limit"
            else if (_val > 75)
                return "warning-level limit"
            else return "good-level limit"
        }
        if (_parameter === "o3") {
            if (_val > 240)
                return "danger-level limit"
            else if (_val > 160)
                return "warning-level limit"
            else return "good-level limit"
        }
        if (_parameter === "no2") {
            if (_val > 200)
                return "danger-level limit"
            else if (_val > 40)
                return "warning-level limit"
            else return "good-level limit"
        }
        if (_parameter === "so2") {
            if (_val > 125)
                return "danger-level limit"
            else if (_val > 50)
                return "warning-level limit"
            else return "good-level limit"
        }
        else {
            return ""
        }
    }
    barColor(_val, _parameter) {

        if (_parameter === "pm25") {
            if (_val > 50)
                return "danger"
            else if (_val > 37.5)
                return "warning"
            else return "good"
        }
        if (_parameter === "pm10") {
            if (_val > 100)
                return "danger"
            else if (_val > 75)
                return "warning"
            else return "good"
        }
        if (_parameter === "o3") {
            if (_val > 240)
                return "danger"
            else if (_val > 160)
                return "warning"
            else return "good"
        }
        if (_parameter === "no2") {
            if (_val > 200)
                return "danger"
            else if (_val > 40)
                return "warning"
            else return "good"
        }
        if (_parameter === "so2") {
            if (_val > 125)
                return "danger"
            else if (_val > 50)
                return "warning"
            else return "good"
        }
        else {
            return "nothing"
        }
    }

    checkLimit(_val, _parameter) {
        if (_parameter === "pm25") {
            return (_val / 50 * 100).toPrecision(4) + "%"
        }
        if (_parameter === "pm10") {
            return (_val / 100 * 100).toPrecision(4) + "%"
        }
        if (_parameter === "o3") {
            return (_val / 240 * 100).toPrecision(4) + "%"
        }
        if (_parameter === "no2") {
            return (_val / 200 * 100).toPrecision(4) + "%"
        }
        if (_parameter === "so2") {
            return (_val / 125 * 100).toPrecision(4) + "%"
        }
        else {
            return "no info"
        }
    }

    render() {
        return (
            <div className="main">
                <div className={this.barColor(this.props.item.result, this.props.item.parameter)} style={{ height: "1vh" }}> &nbsp;</div>
                <div className="margin-bottom-large">
                    <div className="notification city-card is-marginless">
                        <span className="city-text">{this.props.item.city}</span>
                        <br></br>
                        <br></br>
                        <div className="value-text">
                            Value: &nbsp; <span className={this.valueTextColor(this.props.item.result, this.props.item.parameter)}><strong>{(this.props.item.result).toPrecision(4)}</strong></span> &nbsp;
                            {this.state.unit} &nbsp;
                            <br></br>
                            Limit: &nbsp; <strong>{this.checkLimit(this.props.item.result, this.props.item.parameter)}</strong> &nbsp;
                            <br></br>
                            Parameter: &nbsp; <strong>{this.props.item.parameter}</strong>
                            <br></br>
                        </div>
                        <br></br>
                        <Accordion
                            wikidata={this.state.wikidata}
                            city={this.state.city}
                            country={this.state.country}
                            latitude={this.state.latitude ? this.state.latitude : null}
                            longitude={this.state.longitude ? this.state.longitude : null}
                            date={this.state.date}
                            count={this.state.count}
                        />
                    </div>
                </div>
            </div>

        )
    }
}


export default Main