import React from 'react'
import openaq from '../../src/openaq.png'

class Form extends React.Component {

    state = {
        archive: false,
        prevCountry: null,
        prevParameter: null,
        prevAccuracy: null,
    }

    makeArchive = () => {
        this.setState(prevstate => {
            return {
                archive: !prevstate.archive
            }
        })
    }

    componentDidMount() {
        this.setState({
            prevCountry: localStorage.getItem('country'),
            prevParameter: localStorage.getItem('parameter'),
            prevAccuracy: localStorage.getItem('accuracy'),
        })
    }

    clearInput(e) {
        e.target.value = null;
    }
    returnInput(e) {
        if (e.target.name === "country")
            e.target.value = this.state.prevCountry;

        if (e.target.name === "parameter")
            e.target.value = this.state.prevParameter;

        if (e.target.name === "accuracy")
            e.target.value = this.state.prevAccuracy;
    }

    updateInput(e) {
        if (e.target.name === "country") {
            this.setState({
                prevCountry: e.target.value
            })
        }
        if (e.target.name === "parameter")
            this.setState({
                prevParameter: e.target.value
            })
        if (e.target.name === "accuracy")
            this.setState({
                prevAccuracy: e.target.value
            })
    }

    updateStorage() {
        this.setState({
            prevCountry: localStorage.getItem('country'),
            prevParameter: localStorage.getItem('parameter'),
            prevAccuracy: localStorage.getItem('accuracy'),
        })

        return this.state.archive ? this.props.getArchiveData : this.props.getNewData
    }

    submitForm = () => {
        return this.state.archive ? this.props.getArchiveData : this.props.getNewData
    }

    render() {
        return (
            <div onSubmit={() => this.updateStorage()} onChange={(e) => this.updateInput(e)} onBlur={(e) => this.returnInput(e)}>
                <div className="decoration"> &nbsp;</div>
                <form className="notification is-marginless is-paddingless"
                    onSubmit={this.submitForm()}
                    autoComplete="off">
                    <span style={{ paddingTop: "3vh" }} className="center">
                        <input
                            style={{ width: "40%", textAlign: "center" }}
                            placeholder="Country..."
                            defaultValue={this.state.prevCountry}
                            className="input margin-small-vertical form-input"
                            onFocus={(e) => this.clearInput(e)}
                            list="countries"
                            name="country">
                        </input>

                        <input
                            style={{ width: "40%", textAlign: "center" }}
                            placeholder="Parameter..."
                            defaultValue={this.state.prevParameter}
                            className="input margin-small-vertical form-input"
                            onFocus={(e) => this.clearInput(e)}
                            list="parameters"
                            name="parameter">
                        </input>
                    </span>
                    <span className="center">
                        <input
                            style={{ width: "50%", textAlign: "center" }}
                            placeholder="Accuracy from 10-1000"
                            type="number"
                            onFocus={(e) => this.clearInput(e)}
                            onBlur={(e) => this.returnInput(e)}
                            defaultValue={this.state.prevAccuracy}
                            className="input margin-small-vertical form-input"
                            name="accuracy"></input>
                    </span>
                    <datalist id="countries">
                        <option value="Poland">Poland</option>
                        <option value="Germany">Germany</option>
                        <option value="France">France</option>
                        <option value="Spain">Spain</option>
                    </datalist>
                    <datalist id="parameters">
                        <option value="pm25"></option>
                        <option value="pm10"></option>
                        <option value="no2"></option>
                        <option value="co"></option>
                        <option value="o3"></option>
                        <option value="so2"></option>
                    </datalist>
                    <div className="center margin-small-vertical">
                    </div>
                    <div className="center">
                        {!this.props.loaded
                            ? <button className="button center is-info">Search</button>
                            : <button className="button center is-info is-loading">Search</button>}
                    </div>
                    <span className="center"><input onChange={() => this.makeArchive()} type="checkbox" /><label>show old results?</label></span>
                    <span className="center" ><img style={{ marginBottom: "15px", maxHeight: "40px" }} alt="" src={openaq}></img></span>
                </form>
                <div style={{ marginBottom: "35px" }} className="decoration"> &nbsp;</div>
            </div>
        )
    }
}

export default Form