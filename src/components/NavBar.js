import React from 'react'
import logo from '../logo.png';

class NavBar extends React.Component {
  render() {
    return (
      <div className="navbar center">
        <img className="logo" src={logo} alt=""></img>
      </div>
    )
  }
}
export default NavBar