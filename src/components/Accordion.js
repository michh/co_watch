import React from 'react'

function Accordion(props) {

    const rollAccord = (e) => {
        e.target.classList.toggle("is-open");
        var content = e.target.nextElementSibling;
        if (content.style.maxHeight) {
            content.style.maxHeight = null
        } else {
            content.style.maxHeight = 2 * content.scrollHeight + "px";
        }
    }

    const date = new Date(props.date)
    return (
        <div>
            <button onClick={(e) => rollAccord(e)} className="accordion"></button>

            <div className="accordion-content">
                <div className="columns">
                    <div className="column">
                        {props.wikidata ? props.wikidata.replace('(listen)', '') : null}
                        <br></br>
                        {props.wikidata
                            ? <a href={"https://en.wikipedia.org/wiki/" + props.city}>Click to learn more...</a>
                            : <div> Something went wrong...
                                <br></br>
                                --> &nbsp; <a href={"https://google.com/search?q=" + props.city + " wiki en"}>Search it on Google</a>
                                <br></br>
                                --> &nbsp; <a href={"https://www.google.com/maps/search/?api=1&query=" + props.latitude + "," + props.longitude}> Use maps to find</a>
                            </div>}
                        <br></br>
                    </div>
                    <div className="column">
                        Country: {props.country}
                        <br></br>
                        Latitude: {props.latitude ? (props.latitude).toPrecision(6) : null} {props.latitude > 0 ? "N" : "S"}
                        <br></br>
                        Longitude: {props.longitude ? (props.longitude).toPrecision(6) : null} {props.latitude > 0 ? "E" : "W"}
                        <br></br>
                        Last measurement: {date.toUTCString()}
                        <br></br>
                        Number of measurements: {props.count}
                        <br></br>
                    </div>

                </div>
            </div>
        </div>
    )

}
export default Accordion