### about
"co_watch" an app designed to provide current and archive data about air pollution

### tech
- React
- Bulma
- OpenAq API
- MediaWiki API

### running the app
Runs the app in the development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

Also app is hosted at https://preacherxp.github.io/co_watch/.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### usage
3 input forms:
 - country; Poland, Germany, France and Spain supported
 - parameter; pm2.5, pm10, co, so2, no2, o3 supported
 - accuracy; number of items fetched from an openaq api; 10-1000 limit (more is more accurate)
 
 Fetches 10 most polluted cities from speciefied country. By default it gives most recent data, but there is an option to see archive top results.
 
 Keeps input state after reload.
 Provides wiki info about cities.
 
 ### bugs
 - some API invalid calls could result in a block (rare, but fixable)
 - OpenAq API is labeling some of the cities incorrectly
 - for some reason Warsaw cannot be found by MediaWiki API
 - you tell me
 
 ### to improve
 - mobile ui (usable, but could be improved)
 - intruduce more info about air parameters


